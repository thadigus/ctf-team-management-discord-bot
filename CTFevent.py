#################
## CTFevent.py ##
#################
#
# An object for CTF events to be processed and maintained.

class: ctf_event

    # Default initialization varaibles, requires at least a name, start and end date and time.
    def __init__(self, event_name, event_start_datetime, event_end_datetime, event_link = None, event_discord = None, event_team_size = None, event_notes = None):
        self.event_name = event_name
        self.event_start_datetime = event_start_datetime
        self.event_end_datetime = event_end_datetime
        self.event_link = event_link
        self.event_discord = event_discord
        self.event_team_size = event_team_size
        self.event_notes = event_notes
