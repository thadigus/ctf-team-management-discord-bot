# Library imports
try:
    import discord
    import yaml
except:
    print("Unable to import modules necessary for operation. Please perform a `pip3 install -r requirements.txt`")
    exit()

# Load in config from config.yml
def load_config_params(filepath):
    try:
        with open(str(filepath), 'r') as file:
            config_params = yaml.safe_load(file)
        return config_params
    except:
        print(f"Unable to load variables from {str(filepath)}, file may not exist.")
        exit()

# Library initalizations
try:
    intents = discord.Intents.default()
    intents.message_content = True
    client = discord.Client(intents=intents)
    tree = discord.app_commands.CommandTree(client)
except:
    print("Unable to initialize Discord library. Discord.py may not be install properly.")

# Bot ping command to test connection
@tree.command(name = "ping", description = "Test operability of Bot.")
async def ping_test(interaction):
    await interaction.response.send_message("pong")

# Function to parse events in CTF channel
async def parse_events_channel(channel, log_channel):
    # Log event in logging channel
    await log_channel.send(f"Parsing events in {channel.name}")
    # Pull the latest 25 messages from the CTF channel and put them into a list for further processing
    messages = [message async for message in channel.history(limit=25)]
    # Initialize a list of event objects
    events_list = []
    # Iterate through list of messages to create event objects 
    for message in messages:
        pass #TODO Create parser to read all messages in and return a list of event objects.
    # Return list of event objects
    return events_list

# On_ready performed when bot connects for the first time
@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')
    await tree.sync()
    # Create channel objects for both CTF channel and logging channel
    log_channel = client.get_channel(config["LOG_CHANNEL_ID"])
    channel = client.get_channel(config["CHANNEL_ID"])
    await log_channel.send("CTF Bot is Initializing...")
    events_list = await parse_events_channel(channel, log_channel)

# Configuration import
config = load_config_params("config.yml")
# Starts bot
client.run(config["DISCORD_TOKEN"])