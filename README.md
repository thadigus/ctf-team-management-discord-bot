# CTF Team Management Discord Bot

Bot for managing CTF Teams including sign up and role delegation for specific events.

![pipeline status badge](https://gitlab.com/thadigus/ctf-team-management-discord-bot/badges/master/pipeline.svg)

## Installation

Register at the [Discord Developer Portal](https://discord.com/developers/applications) and create a new application.

Fill out details on application to your own desire.

Add a bot in the Bot tab and reset token to get the Discord token required for operation.

Enable all privileged gateway intents on Bot page.

Go to OAuth2 -> URL Generator tab. Enable the `bot` scope and then add all permissions you feel comfortable adding for this API key. Then copy the url at the bottom and visit it with your authenticated Discord account in your browser to add the bot to your server.

Enable developer mode in your Discord client and [get the channel ID](https://turbofuture.com/internet/Discord-Channel-ID) for the dedicated channel to display the uptime dashboard.

Download and build the bot.

```bash
git clone git@gitlab.com:thadigus/ctf-team-management-discord-bot.git`
cd ctf-team-management-discord-bot
cp example-config.yml config.yml
vi config.yml
```

Open config.yml and add Discord key for the bot and the channel ID for the channel to dedicate to the bot (all content will be deleted). Add services and admins as shown in [example](#usage).

### Running with Python (Debugging)

Below are the commands to setup and run the bot with Python. This is great for debugging the config.yml file and testing the bot in general. You can do this to ensure that everything is working as intended before submitting an issue, and to ensure that your config.yml file is working as intended before deploying to production.

```bash
pip install -r requirements.txt
python main.py
```

### Running in Docker Container (Recommended)

```bash
docker build -t ctf-team-management-discord-bot .
docker run -d --name ctf-team-management-discord-bot ctf-team-management-discord-bot
docker ps
```

## Usage

**Config.yml Example**

```yaml
---
DISCORD_TOKEN: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ-C' # Discord API key for bot created in developer portal.
CHANNEL_ID: 12345678901234567890 # Channel ID of dedicated channel for bot to post current uptime.
```

## Support

Please open an [issue](https://gitlab.com/thadigus/custom-services-uptime-discord-bot/-/issues/new) if you have any problems. I will prioritize and help as I can.

## Roadmap

Please create issues for feature requests or any ideas. I will prioritize them and add them if they are a good fit.

## Authors and acknowledgment

### [Thad Turner](https://thadigus.gitlab.io/) - Cybersecurity Practitioner

### [Eric Gaby](https://www.linkedin.com/in/eric-gaby-mba-a64439194/) - Security Engineer

## License

There is no current license specification for this project.
